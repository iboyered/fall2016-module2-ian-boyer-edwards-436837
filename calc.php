<!DOCTYPE html>
    <head><title>Calculation</title>
    <meta charset="utf-8"/>
    </head>
    <body>
        <?php
        $num1 = (int) $_GET['num1'];
        $num2 = (int) $_GET['num2'];
        $operation = $_GET['operation'];
        
        if ($operation == "add") {
            echo $num1 + $num2;
        } elseif ($operation == "subtract") {
            echo $num1 - $num2;
        } elseif ($operation == "multiply") {
            echo $num1 * $num2;
        } elseif ($operation == "divide") {
            echo $num1 / $num2;
        }
        
        ?>
    </body>
</html>